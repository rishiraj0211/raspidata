import paho.mqtt.client as paho
import ssl
from time import sleep
from random import uniform
import json
import csv
import os
from scapy.all import *
from datetime import datetime
import threading
import pause

data = {}
lock = threading.Lock()

def publish():
	lock.acquire()
	print(time.ctime())
	for key in data:
		client.publish("prototype/sensor/sensor2", json.dumps(data[key]), qos=1)
	data.clear()
	lock.release()
	threading.Timer(1, publish).start()

def analyze(packet):
	lock.acquire()
	mac = packet.getlayer(Dot11).addr2

	if(mac is not None):
		data[mac] = {}
		data [mac]['MAC_ADDRESS'] = packet.getlayer(Dot11).addr2
		data [mac]['TIMESTAMP'] = int(float(packet.time) * 1000)
		data [mac]['SIG_STRENGTH'] = min(0,int(packet.dBm_AntSignal))

	lock.release()
#		client.publish("prototype/sensor/sensorTest", json.dumps(data[mac]), qos=1)
#		print("msg sent: asdf")

def on_connect(client,userdata, flags, rc):
	print("Connection returned result: " + str(rc))

client = paho.Client()
client.on_connect = on_connect

awshost = "a1o9ljzxje2g1i-ats.iot.us-east-2.amazonaws.com"
awsport = 8883
clientId = "RasPi2"
thingName = "RasPi2"
caPath = "certs/certificate.crt"
certPath = "certs/a6c76cc842-certificate.pem.crt"
keyPath = "certs/a6c76cc842-private.pem.key"

client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
client.connect(awshost, awsport, keepalive=60)

channel = 6
os.system("sudo iwconfig %s channel %d" % ("wlan1mon",channel))
#pause.until(datetime(2020,4,18,12,30,00,00))
publish()
client.loop_start()

while 1==1:
	sniff(iface="wlan1mon",prn=analyze)
