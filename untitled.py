import paho.mqtt.client as paho
import ssl
from time import sleep
from random import uniform
import json
import csv
import os
from scapy.all import *
from datetime import datetime
import threading

data = {}
lock = threading.Lock()

def publish():
	lock.acquire()
	print(time.ctime())
	for key in data:
		client.publish("prototype/sensor/sensor1", json.dumps(data[key]), qos=1)
	lock.release()
	threading.Timer(1, publish).start()

def analyze(packet):
	lock.acquire()
	mac = packet.getlayer(Dot11).addr2
	if(mac == "e8:e8:b7:5f:fa:e8" or mac == "a0:cc:2b:a3:77:41" and mac != "dc:a6:32:13:28:c6"):
		data[mac] = {}
		data [mac]['MAC_ADDRESS'] = packet.getlayer(Dot11).addr2
		data [mac]['TIMESTAMP'] = int(float(packet.time) * 1000)
		data [mac]['SIG_STRENGTH'] = packet.dBm_AntSignal
	lock.release()
#		client.publish("prototype/sensor/sensorTest", json.dumps(data[mac]), qos=1)
#		print("msg sent: asdf")

def on_connect(client,userdata, flags, rc):
	print("Connection returned result: " + str(rc))

client = paho.Client()
client.on_connect = on_connect

awshost = "a1o9ljzxje2g1i-ats.iot.us-east-2.amazonaws.com"
awsport = 8883
clientId = "RasPi1"
thingName = "RasPi1"
caPath = "certs/certificate.crt"
certPath = "certs/5ffc68ed63-certificate.pem.crt"
keyPath = "certs/5ffc68ed63-private.pem.key"

client.tls_set(caPath, certfile=certPath, keyfile=keyPath, cert_reqs=ssl.CERT_REQUIRED, tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
client.connect(awshost, awsport, keepalive=60)

channel = 6
os.system("sudo iwconfig %s channel %d" % ("wlan1mon",channel))

publish()
client.loop_start()

while 1==1:
	sniff(iface="wlan1mon",prn=analyze)

	
	




























